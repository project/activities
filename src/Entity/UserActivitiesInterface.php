<?php

namespace Drupal\activities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining User activities entities.
 *
 * @ingroup activities
 */
interface UserActivitiesInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the User activities creation timestamp.
   *
   * @return int
   *   Creation timestamp of the User activities.
   */
  public function getCreatedTime();

  /**
   * Sets the User activities creation timestamp.
   *
   * @param int $timestamp
   *   The User activities creation timestamp.
   *
   * @return \Drupal\activities\Entity\UserActivitiesInterface
   *   The called User activities entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the related entity id.
   *
   * @return string
   */
  public function getRelatedEntityId();

  /**
   * Sets the related entity id
   *
   * @param string $entity_id
   *
   * @return this
   */
  public function setRelatedEntityId($entity_id);

  /**
   * Gets the related entity type id
   *
   * @return string
   */
  public function getRelatedEntityTypeId();

  /**
   * Sets the related entity type id
   *
   * @param string $entity_type_id
   *
   * @return this
   */
  public function setRelatedEntityTypeId($entity_type_id);

  /**
   * Gets the operation of the activities.
   *
   * @return string
   */
  public function getOperation();

  /**
   * Sets the operation.
   *
   * @param string $operation
   *
   * @return this
   */
  public function setOperation($operation);

  /**
   * Gets additonal info about the activities.
   *
   * @return string
   */
  public function getInfo();

  /**
   * Sets additional info about the activities.
   *
   * @param string $info
   * 
   * @return this
   */
  public function setInfo($info);

  /**
   * Gets the issuer's IP Address
   *
   * @return string
   */
  public function getIpAddress();

  /**
   * Sets the issuer's IP Address
   *
   * @param string $ip_address
   *
   * @return this
   */
  public function setIpAddress($ip_address);


  /**
   * Gets the location.
   * 
   * @return string
   */
  public function getLocation();

  /**
   * Sets the location
   *
   * @param string $location
   * 
   * @return this
   */
  public function setLocation($location);
}
