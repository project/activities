<?php

namespace Drupal\activities\Entity;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the User activities entity.
 *
 * @ingroup activities
 *
 * @ContentEntityType(
 *   id = "user_activities",
 *   label = @Translation("User activities"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\activities\UserActivitiesListBuilder",
 *     "views_data" = "Drupal\activities\Entity\UserActivitiesViewsData",
 * 
 *     "access" = "Drupal\activities\UserActivitiesAccessControlHandler",
 *   },
 *   base_table = "user_activities",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uid" = "user_id"
 *   },
 * )
 */
class UserActivities extends ContentEntityBase implements UserActivitiesInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function label() {
    $entity_id = $this->getRelatedEntityId();
    $entity_type_id = $this->getRelatedEntityTypeId();

    try {
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
      return $entity ? $entity->label() : $this->id();
    } catch (PluginNotFoundException $e) {
      return $this->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getRelatedEntityId() {
    return $this->get('entity_id')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setRelatedEntityId($entity_id) {
    $this->set('entity_id', $entity_id);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getRelatedEntityTypeId() {
    return $this->get('entity_type_id')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setRelatedEntityTypeId($entity_type_id) {
    $this->set('entity_type_id', $entity_type_id);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getOperation() {
    return $this->get('operation')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    return $this->get('info')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setInfo($info) {
    $this->set('info', $info);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setOperation($operation) {
    $this->set('operation', $operation);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getIpAddress() {
    return $this->get('ip')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setIpAddress($ip_address) {
    $this->set('ip', $ip_address);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocation() {
    return $this->get('location')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function setLocation($location) {
    $this->set('location', $location);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the User activities entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['operation'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Operation'))
      ->setDescription(t('Describes what happened in this activities.'))
      ->setSettings(array(
        'allowed_values' => [
          'delete' => t('Delete'),
          'create' => t('Create'),
          'insert' => t('Create'),
          'update' => t('Update')
        ],
      ))
      ->setRequired(TRUE);

    $fields['info'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Info'))
      ->setDescription(t('Additonal info about the activities.'));

    $fields['ip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP Address'))
      ->setDescription(t('The IP address of the issuer.'))
      ->setRequired(TRUE);

    $fields['location'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Location'))
      ->setDescription(t('The location of the event.'))
      ->setRequired(TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Related Entity ID'))
      ->setDescription(t('The entity ID related to the activities.'))
      ->setRequired(TRUE);

    $fields['entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Related Entity Type ID'))
      ->setDescription(t('The entity type ID related to the activities.'))
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }
}
