<?php

namespace Drupal\activities\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for User activities entities.
 */
class UserActivitiesViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['user_activities']['description'] = [
      'title' =>  $this->t('Description'),
      'help' => $this->t('The description of the event'),
      'field' => [
        'id' => 'description_field',
      ],
    ];

    $data['user_activities']['related_entity_link'] = [
      'title' => $this->t('Related Entity Link'),
      'help' => $this->t('The canonical link of related entity'),
      'field' => [
        'id' => 'related_entity_link',
      ],
    ];

    $data['user_activities']['location']['field']['id'] = 'event_link';

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }
}
