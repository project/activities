<?php

namespace Drupal\activities\Plugin\views\field;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use InvalidArgumentException;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("related_entity_link")
 */
class RelatedEntityLinkField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $table = $this->ensureMyTable();
    $aliases = [];
    $aliases['operation'] = $query->addField($table, 'operation');
    $aliases['entity_id'] = $query->addField($table, 'entity_id');
    $aliases['entity_type_id'] = $query->addField($table, 'entity_type_id');
    $aliases['info'] = $query->addField($table, 'info');

    $this->aliases = $aliases;
  }

  /**
   * {@inheritdoc}
   */
  public function advancedRender(ResultRow $values) {
    $entity_id = $this->getValue($values, 'entity_id');
    $entity_type_id = $this->getValue($values, 'entity_type_id');

    try {
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    } catch (PluginNotFoundException $e) {
      return '';
    }


    if ($entity instanceof EntityInterface) {
      try {
        $link = $entity->toLink(
          $entity->toUrl('canonical', ['absolute' => TRUE])->toString(),
          'canonical',
          ['absolute' => TRUE]
        );

        return $link->toString();
      } catch (UndefinedLinkTemplateException $e) {
      }
    }

    return $this->t('- None -');
  }
}
