<?php

namespace Drupal\activities\Plugin\views\field;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use InvalidArgumentException;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("event_link")
 */
class EventLinkField extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function advancedRender(ResultRow $values) {
    $value = $this->getValue($values);

    try {
      $url = Url::fromUserInput($value, ['absolute' => TRUE]);
      return Link::fromTextAndUrl($url->toString(), $url)->toString();
    } catch (\InvalidArgumentException $e) {
      return $this->t('- None -');
    }
  }
}
