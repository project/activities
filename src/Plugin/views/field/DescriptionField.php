<?php

namespace Drupal\activities\Plugin\views\field;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use InvalidArgumentException;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("description_field")
 */
class DescriptionField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $table = $this->ensureMyTable();
    $aliases = [];
    $aliases['operation'] = $query->addField($table, 'operation');
    $aliases['entity_id'] = $query->addField($table, 'entity_id');
    $aliases['entity_type_id'] = $query->addField($table, 'entity_type_id');
    $aliases['info'] = $query->addField($table, 'info');

    $this->aliases = $aliases;
  }

  /**
   * {@inheritdoc}
   */
  public function advancedRender(ResultRow $values) {
    $op = $this->getValue($values, 'operation');
    $entity_id = $this->getValue($values, 'entity_id');
    $entity_type_id = $this->getValue($values, 'entity_type_id');
    $info = $this->getValue($values, 'info');
    try {
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
      $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    } catch (PluginNotFoundException $e) {
      return '';
    }


    $description = NULL;
    switch ($op) {
      case 'insert':
      case 'create':
        $description = 'Create :entity_type: %entity';
        break;
      case 'update':
        $description = 'Update :entity_type: %entity';
        break;
      case 'delete':
        $description = 'Delete :entity_type: %entity';
        break;
    }

    if ($op === 'delete' || !$entity) {
      $link = sprintf('%s (%s)', $info, $entity_id);
    } else {
      try {
        $link = $entity->toLink(sprintf('%s (%s)', $entity->label(), $entity->id()))->toString();
      } catch (UndefinedLinkTemplateException $e) {
        $link = sprintf('%s (%s)', $entity->label(), $entity->id());
      }
    }

    $description = $this->t($description, [':entity_type' => $entity_type->getLabel(), '%entity' => $link]);

    return $description;
  }
}
