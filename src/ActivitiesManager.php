<?php

namespace Drupal\activities;

use Drupal\activities\Entity\UserActivitiesInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;

/**
 * Class ActivitiesManager.
 */
class ActivitiesManager {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ActivitiesManager object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get All Content Entity.
   */
  public function getContentEntity() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type) {
      if ($entity_type->getGroup() === 'content' && $entity_type->id() !== 'user_activities') {
        $content_entity_types[$entity_type->id()] = $entity_type->getLabel();
      }
    }
    return $content_entity_types;
  }

  /**
   * Get Description for log.
   */
  public function activitiesGetDescription(UserActivitiesInterface $user_activities) {
    $op = $user_activities->getOperation();
    $entity_id = $user_activities->getRelatedEntityId();
    $entity_type_id = $user_activities->getRelatedEntityTypeId();
    $info = $user_activities->getInfo();
    try {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    }
    catch (PluginNotFoundException $e) {
      return '';
    }

    $description = NULL;
    switch ($op) {
      case 'insert':
      case 'create':
        $description = 'Create :entity_type: %entity';
        break;

      case 'update':
        $description = 'Update :entity_type: %entity';
        break;

      case 'delete':
        $description = 'Delete :entity_type: %entity';
        break;
    }

    if (!$description) {
      return '';
    }

    if ($op === 'delete' || !$entity) {
      $link = sprintf('%s (%s)', $info, $entity_id);
    }
    else {
      try {
        $link = $entity->toLink(sprintf('%s (%s)', $entity->label(), $entity->id()))->toString();
      }
      catch (UndefinedLinkTemplateException $e) {
        $link = sprintf('%s (%s)', $entity->label(), $entity->id());
      }
    }

    $description = t($description, [
      ':entity_type' => $entity_type->getLabel(),
      '%entity' => $link,
    ]);

    return $description;
  }

  /**
   * Get Link For Logs.
   */
  public function activitiesGetLink(UserActivitiesInterface $user_activities) {
    $entity_id = $user_activities->getRelatedEntityId();
    $entity_type_id = $user_activities->getRelatedEntityTypeId();

    try {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
    }
    catch (PluginNotFoundException $e) {
      return '';
    }

    if ($entity instanceof EntityInterface) {
      try {
        $link = $entity->toLink(
        $entity->toUrl('canonical', ['absolute' => TRUE])->toString(),
        'canonical',
        ['absolute' => TRUE]
        );

        return $link->toString();
      }
      catch (UndefinedLinkTemplateException $e) {
      }
    }

    return t('- None -');
  }

}
