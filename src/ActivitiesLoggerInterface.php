<?php

namespace Drupal\activities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
/**
 * Interface ActivitiesLoggerInterface.
 */
interface ActivitiesLoggerInterface {

  /**
   * Logs an activities
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $op
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\activities\Entity\UserActivitiesInterface
   */
  public function log(EntityInterface $entity, $op, AccountInterface $account = NULL);
}
