<?php

namespace Drupal\activities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ActivitiesLogger.
 */
class ActivitiesLogger implements ActivitiesLoggerInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   * */
  protected $request;

  /**
   * Constructs a new ActivitiesLogger object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $account, RequestStack $request) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $account;
    $this->request = $request->getMainRequest();
  }

  /**
   * Prepares a user  .
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\user\UserInterface
   */
  protected function prepareUser(AccountInterface $account = NULL) {
    if (!$account) {
      $account = $this->currentUser;
    }

    return $this->entityTypeManager->getStorage('user')->load($account->id());
  }

  /**
   * {@inheritDoc}
   */
  public function log(EntityInterface $entity, $op, ?AccountInterface $account = NULL) {
    $user = $this->prepareUser($account);
    if ($user) {
      /** @var \Drupal\activities\Entity\UserActivitiesInterface $activities */
      $activities = $this->entityTypeManager->getStorage('user_activities')->create();
      $activities->setOwner($user);
      $activities->setRelatedEntityId($entity->id());
      $activities->setRelatedEntityTypeId($entity->getEntityTypeId());
      $activities->setOperation($op);
      $activities->setIpAddress($this->request->getClientIp());
      $activities->setInfo($entity->label());
      $activities->setLocation($this->request->getRequestUri());

      $activities->save();
      return $activities;
    }

    return NULL;
  }

}
