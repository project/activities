<?php

namespace Drupal\activities\Form;

use Drupal\activities\ActivitiesManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Activities Allowed Type.
 */
class ActivitiesAllowedType extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The activity Manager.
   *
   * @var \Drupal\activities\ActivitiesManager
   */
  protected $activityManager;

  /**
   * Constructs a new form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\activities\ActivitiesManager $activity_manager
   *   The activity Managerr.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ActivitiesManager $activity_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->activityManager = $activity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('activities.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activity_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('activities.settings');
    $content_entities = $this->activityManager->getContentEntity() ?? [];
    foreach ($content_entities as $key => $value) {
      $form[$key] = [
        '#type' => 'fieldset',
        '#title' => $value,
        '#tree' => TRUE,
        '#collapsible' => TRUE,
      ];
      $form[$key]['options'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Allowed Options For Entity ') . $value,
        '#description' => $this->t('Options for logging the entity.'),
        '#default_value' => $config->get($key),
        '#options' => [
          'create' => t('Create'),
          'delete' => t('Delete'),
          'update' => t('Update'),
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('activities.settings');
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value["options"]);
    }
    $config->save();
  }

}
